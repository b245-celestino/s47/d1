// Syntax: document.querySelector("html element")

let universalSelector = document.querySelectorAll("*");

let singleUniversalSelector = document.querySelector("*");

console.log(universalSelector);
console.log(singleUniversalSelector);

let classSelector = document.querySelectorAll(".full-name");

console.log(classSelector);

let singleClassSelector = document.querySelector(".full-name");

console.log(singleClassSelector);

let tagSelector = document.querySelectorAll("input");

console.log(tagSelector);

let spanSelector = document.querySelector("span[id]");

console.log(spanSelector);

let txtFirstName = document.querySelector("#txt-first-name");

// Add event listener
txtFirstName.addEventListener("keyup", () => {
  console.log(txtFirstName.value);
  spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
});

let txtLastName = document.querySelector("#txt-last-name");

txtLastName.addEventListener("keyup", () => {
  spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
});

// ACTIVITY s47

document.querySelector("#text-color").onchange = () => {
  selectedColor();
};

function selectedColor() {
  let selected = document.querySelector("#text-color");
  fullName.style.color = selected.value;
}
